import java.util.*;

public class Mancala {

    // Representing Holes in Mancala
    // Index 0 - 5      = Small Hole for player 1
    // Index 6          = Big Hole for player 1
    // Index 7 - 12     = Small Hole for player 2
    // Index 13         = Big Hole for player 2
    private int[] Holes = new int[14];

    // Constructor
    public Mancala() {
        for (int i = 0; i < this.Holes.length; i++) {
            if (!LumbungHole(i)) {
                this.Holes[i] = 6;
            } else {
                this.Holes[i] = 0;
            }
        }
    }

    // GetAllHole to get state from mancala
    public int[] GetAllHole() {
        return this.Holes;
    }

    // GetPlayerOneScore to get score from player 1
    public int GetPlayerOneScore() {
        return this.Holes[6];
    }

    // GetPlayerTwoScore to get score from player 2
    public int GetPlayerTwoScore() {
        return this.Holes[13];
    }

    // ResetOneHole is to reset one target hole
    public void ResetOneHole(int hole) {
        if (hole > this.Holes.length) {
            System.out.println("Out of range");
        } else {
            this.Holes[hole] = 0;
        }
    }

    // ResetAllHole is to reset all hole
    public void ResetAllHole() {
        for (int i = 0; i < this.Holes.length; i++) {
            Holes[i] = 0;
        }
    }

    // AddOneHole is to add one rock to one target hole
    public void AddOneHole(int hole) {
        if (hole > this.Holes.length) {
            System.out.println("Out of range");
        } else {
            this.Holes[hole]++;
        }
    }

    // PlayerMove is player move
    public void PlayerMove(int player, int hole) {
        if (this.Holes[hole] != 0) {
            
        } else {
            System.out.println("Invalid Move");
        }
    }

    //
    public boolean ValidMove(int player, int currentHole) {
        if (player == 1) {
            int nextHole = currentHole + 1;
            if (nextHole != 13) {
                return true;
            }
            return false;
        } else if (player == 2) {
            int nextHole = currentHole + 1;
            if (nextHole != 6) {
                return true;
            }
            return false;
        }
        return false;
    }

    //
    public boolean CheckNextHole(int player, int currentHole) {
        if (player == 1) {
            int nextHole = this.MoveIterator(currentHole);
            if (nextHole != 13) {
                return true;
            }
            return false;
        } else if (player == 2) {
            int nextHole = this.MoveIterator(currentHole);
            if (nextHole != 6) {
                return true;
            }
            return false;
        }
        return false;
    }

    public int MoveIterator(int currentHole) {
        if (currentHole + 1 > 13) {
            return 0;
        }
        currentHole++;
        return currentHole;
    }

    // PrintMancala is to print mancala
    public void PrintMancala() {
        System.out.print("\t");
        for (int i = 7; i < 13; i++) {
            System.out.print(this.Holes[i] + "\t");
        }
        System.out.println();
        System.out.print(this.Holes[6] + "\t\t\t\t\t\t\t" + this.Holes[13]);
        System.out.println();
        System.out.print("\t");
        for (int i = 5; i >= 0 ; i--) {
            System.out.print(this.Holes[i] + "\t");
        }
        System.out.println();
    }

    // LumbungHole is to check whether hole is lumbung or not
    private static boolean LumbungHole(int hole) {
        switch (hole){
            case 6:
                return true;
            case 13:
                return true;
            default:
                return false;
        }
    }
}