
JFLAGS = -g
JC = javac
J = java
.SUFFIXES: .java .class
.java.class: $(JC) $(JFLAGS) $*.java

CLASSES = \
    Mancala.java \
    Main.java 

default: classes

run:
	$(JC) *.java && $(J) Main

compile:
	$(JC) *.java

classes: 
	$(CLASSES:.java=.class)

clean:
	$(RM) *.class